import re

def main():
	exit = False
	while exit == False:
		print("..::Welcome to Pymandb::..")
		print("The data base management system built with python")
		print("1) Input data")
		print("2) Read data")
		print("3) Change data")
		print("4) Delete data")
		print("5) Salir")
		print("----------------------")
		print("Select option: ")
		menuOption = input()
		print("----------------------")
		
		if menuOption == "1":
			create()
		elif menuOption == "2":
			read()
		elif menuOption == "3":
			update()
		elif menuOption == "4":
			delete()
		elif menuOption =="5":
			exit = True

		print("----------------------")

def create():
	filepath = "./pymandb.txt"
	file = open(filepath, "a")
	newItem = []
	joiner = ", "
	
	newId = str(getLastId() + 1)

	if not validateId(newId): # revisa que no exista el id
		print("El ID no existe, bien!")
		newItem.append(newId)

	print("Nombre: ")
	nombre = input()
	newItem.append(nombre)

	print("Apellido paterno: ")
	appaterno = input()
	newItem.append(appaterno)

	print("Apellido materno: ")
	apmaterno = input()
	newItem.append(apmaterno)

	print("fecha de alta(dd/mm/aaaa): ")
	fealta = input()

	if validateDate(fealta):
		newItem.append(fealta)
	else:
		print("Formato de fecha incorrecto")

	print("credito: ")
	credito = input()

	if validateAmount(credito): 
		newItem.append(credito)
	else:
		print("Cantidad invalida")

	print("deuda: ")
	deuda = input()

	if validateAmount(deuda): 
		newItem.append(deuda)
	else:
		print("Cantidad invalida")

	print(newItem)

	print(joiner.join(newItem))
	file.write("\n" + joiner.join(newItem))

	file.close()

def read():
	print("Selecciona una opcion:")
	print("1) Mostrar todos los registros")
	print("2) Mostrar un registro en especifico")
	print("Selecciona una opcion:")
	readOption = input()

	if readOption == "1":
		readAll()
	elif readOption == "2":
		print("Buscar por:")
		print("1) ID")
		print("2) Nombre")
		readSpecificOption = input()

		if readSpecificOption == "1":
			print("Ingresa el id:")
			readId = input()
			readById(readId)
		elif readSpecificOption == "2":
			print("Ingresa el nombre:")
			readName = input()
			readByName(readName)

def update():
	items = fileToArray()
	print("Ingresa el ID del registro a modificar:")
	idToChange = input()
	
	if validateId(idToChange):
		index = getIdIndex(idToChange)

	print("Selecciona el valor que deseas cambiar")
	print("1) Nombre")
	print("2) Apellido paterno")
	print("3) Apellido materno")
	print("4) Fecha de alta")
	print("5) Credito")
	print("6) Deuda")
	updateOption = input()

	if int(updateOption) == 1:
		print("Ingresa un el nuevo nombre:")
		newValue = input()
		items[index][1] = newValue
	elif int(updateOption) == 2:
		print("Ingresa un el nuevo apellido paterno:")
		newValue = input()
		items[index][2] = newValue
	elif int(updateOption) == 3:
		print("Ingresa un el nuevo apellido materno:")
		newValue = input()
		items[index][3] = newValue
	elif int(updateOption) == 4:
		print("Ingresa la nueva fecha de alta(dd/mm/aaa):")
		newValue = input()
		items[index][4] = newValue
	elif int(updateOption) == 5:
		print("Ingresa un el nuevo valor del credito:")
		newValue = input()
		items[index][5] = newValue
	elif int(updateOption) == 6:
		print("Ingresa un el nuevo valor del credito:")
		newValue = input()
		items[index][6] = newValue

	print(items)
	itemsToWrite = []
	joinerComma = ","
	for item in items:
		itemsToWrite.append(joinerComma.join(item))

	joinerNoSpace = ""
	textToWrite = joinerNoSpace.join(itemsToWrite)

	filepath = "./pymandb.txt"
	file = open(filepath, "w")
	file.write(textToWrite)
	file.close()

def delete():
	items = fileToArray()
	print ("Ingresa el ID del registro que deseas eliminar")
	idToDelete = input()
	if validateId(idToDelete):
		index = getIdIndex(idToDelete)
		items.pop(index)
	else:
		print("ID no valido")

	itemsToWrite = []
	joinerComma = ","
	for item in items:
		itemsToWrite.append(joinerComma.join(item))

	joinerNoSpace = ""
	textToWrite = joinerNoSpace.join(itemsToWrite)

	filepath = "./pymandb.txt"
	file = open(filepath, "w")
	file.write(textToWrite)
	file.close()

def fileToArray():
	filepath = "./pymandb.txt"
	file = open(filepath, "rt")
	items = []
	lines = file.readlines()
	
	for line in lines:
		items.append(line.split(","))

	file.close()

	return items

def getLastId():
	items = fileToArray()
	lastId = int(items[len(items) - 1][0]) # Obtiene el número de registros y le resta 1 para seleccionar el último registro, obtiene el id(el primer elemento, del último elemento) de éste registro y lo transforma de string a entero para poder ser utilizado matemáticamente
	
	return lastId

def validateId(idToValidate):
	items = fileToArray()
	validatorValue = False
	
	for item in items:
		if item[0] == idToValidate:
			validatorValue = True

	return validatorValue

def validateDate(dateToValidate):
	regEx = re.search("([012][0-9]|3[01])\/(0[0-9]|1[012])\/[12][0-9][0-9][0-9]", dateToValidate)
	
	if regEx == None:
		validatorDate = False
	else:
		validatorDate = True

def validateAmount(amountToValidate):
	if amountToValidate <= 0:
		return True
	else:
		return False

def getIdIndex(idToGetIndex):
	items = fileToArray()

	for item in items:
		if item[0] == idToGetIndex:
			index = items.index(item)
	return index

def readAll():
	filepath = "./pymandb.txt"
	file = open(filepath, "r")
	print(file.read())
	file.close()

def readById(readId):
	items = fileToArray()
	joiner = ","
	
	if validateId(readId):
		idIndex = getIdIndex(readId)
		print("idcliente, nombre, appaterno, apmaterno, fechaalta, credito, deuda")
		print(joiner.join(items[idIndex]))
	else:
		print("ID no encontrado")

def readByName(readName):
	items = fileToArray()
	itemsFound = []
	itemsJoined = []
	joinerComma = ","
	joinerNoSpace = ""
	
	for item in items:
		if item[1].strip() == readName:
			itemsFound.append(item)
	
	if len(itemsFound) > 0:
		for item in itemsFound:
			itemsJoined.append(joinerComma.join(item))
		
		print(joinerNoSpace.join(itemsJoined))
	else:
		print("Nombre no encontrado")

main()
